var lock_user = {};

/**
 * 接口主函数
 * @param {Object} ctx HTTP上下文
 * @param {Object} db 数据管理器,如: { next: async function{}, ret: {} }
 * @return {Object} 执行结果
 */
async function main(ctx, db) {
	// 获取请求参数
	var req = ctx.request;
	var {
		query,
		body
	} = req;

	var {
		article_id
	} = Object.assign({}, query, body);
	
	var user = await this.get_state(ctx, db);
	// 获取请求数据
	var user_id = user.user_id;

	// 建立数据库操作分支
	var db2 = Object.assign({}, db);
	db2.table = "cms_article";

	var db3 = Object.assign({}, db);
	db3.table = "user_count";
	db3.key = "user_id";

	var db4 = Object.assign({}, db);
	db4.table = "cms_record";

	// 查询各表信息
	var obj = await db2.getObj({
		article_id
	})
	
	var count = await db3.getObj({
		user_id
	})
	
	var rec = await db4.getObj({
		article_id,
		user_id
	});

	// 校验可能出现的问题
	if (rec) {
		return $.ret.bl(true, "用户已购买该文章请勿重复购买。")
	} else if (!obj) {
		console.error(`文章ID: ${article_id} 对应的文章信息不存在！`)
		return $.ret.bl(false, "文章不存在或者已下架！")
	} else if (!count) {
		console.error(`用户ID: ${user_id} 对应的用户统计信息不存在！`)
		return $.ret.bl(false, "查询不到用户积分信息！")
	} else if (count.credit_points < obj.price) {
		return $.ret.bl(false, "用户积分不足请充值或者看广告")
	}

	// 扣除积分
	count.credit_points -= obj.price;

	// 添加购买记录
	db4.add({
		article_id,
		user_id,
		"price": obj.price
	})
	// 返回结果
	return $.ret.bl(true, "购买成功！");
};

exports.main = main;